//
//  ViewController.swift
//  LearningProject
//
//  Created by ignacio on 30-10-20.
//

import UIKit

class ViewController: UIViewController {
    //lazy var is not initialized until some one needs to use it
    //or could be initialized later
    private lazy var game: Concentration =
        Concentration(numberOfPairsOfCards: numberOfPairsOfCards)
    
    var numberOfPairsOfCards: Int {
        return (cardButtons.count + 1) / 2
    }
    
    @IBOutlet private weak var flipCountLabel: UILabel! {
        didSet {
            updateFlipCountLabel()
        }
    }

    @IBOutlet private weak var scoreLabel: UILabel?
    
    @IBOutlet private weak var newGameButton: UIButton?
    
    @IBOutlet private var cardButtons: [UIButton]!
    
    @IBAction private func NewGamePressed() {
        game = Concentration(numberOfPairsOfCards: numberOfPairsOfCards)
        
        emoji.removeAll()
        let themeChoices = Array(emojiThemes.keys)
        let chosenTheme: String = themeChoices[themeChoices.count.arc4random]
        
        emojiChoices = emojiThemes[chosenTheme] ?? ""
        
        print("emoji: \(emoji)")

        updateViewFromModel()
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        if let cardNumber = cardButtons.firstIndex(of: sender) {
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("chosen card was not in cardbuttons")
        }
    }

    private func updateViewFromModel() {
        for index in cardButtons.indices {
            let button = cardButtons[index]
            let card = game.cards[index]
            
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }

        updateFlipCountLabel()
        scoreLabel?.text = "Score: \(game.score)"

        if game.isGameOver {
            newGameButton?.isHidden = false
        } else {
            newGameButton?.isHidden = true
        }
    }
    
    private func updateFlipCountLabel() {
        let attributes: [NSAttributedString.Key: Any] = [
            .strokeWidth: 5.0,
            .strokeColor: #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
        ]
        
        let attributedString = NSAttributedString(string: "Flips: \(game.flipCount)", attributes: attributes)
        flipCountLabel.attributedText = attributedString
    }
    private var emojiThemes: Dictionary = ["halloween" : "🦇😱🙀😈🎃👻🍭🍬🍎",
                                   "food" : "🌭🍔🍟🍕🥪🌮🍗🍤🥩",
                                   "vehicles" : "🚗🚕🚙🚌🚒🚓🚑🚛🚜"]
    
    private lazy var emojiChoices: String = emojiThemes["halloween"] ?? ""
    
    private var emoji = [Card: String]()
    
    private func emoji(for card: Card) -> String {
        if emoji[card] == nil, emojiChoices.count > 0 {
            let randomStringIndex = emojiChoices.index(emojiChoices.startIndex, offsetBy: emojiChoices.count.arc4random)
            emoji[card] = String(emojiChoices.remove(at: randomStringIndex))
        }
        return emoji[card] ?? "?"
    }
}

extension Int {
    var arc4random: Int {
        if self > 0 {
            return Int(arc4random_uniform(UInt32(self)))
        } else if self < 0 {
            return -Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
