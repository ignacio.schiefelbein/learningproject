//
//  Card.swift
//  LearningProject
//
//  Created by ignacio on 01-11-20.
//

import Foundation

struct Card: Hashable
{
    //implementation of 2 Hashable properties
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    

    var isFaceUp = false
    var isMatched = false
    var wasPreviouslySeen = false
    private var identifier: Int
    
    private static var identifierFactory = 0
    
    private static func getUniqueIdentifier() -> Int {
        //in a static method, we can access static vars
        identifierFactory += 1
        return identifierFactory
    }
    
    init() {
        self.identifier = Card.getUniqueIdentifier()
    }
}


