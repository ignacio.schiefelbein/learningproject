//
//  Concentrarion.swift
//  LearningProject
//
//  Created by ignacio on 01-11-20.
//

import Foundation

struct Concentration
{
    private(set) var cards = [Card]()

    private var indexOfOneAndOnlyFaceUpCard: Int? {
        get {
            return cards.indices.filter {cards[$0].isFaceUp}.oneAndOnly
        }
        set(newValue) {
            for index in cards.indices {
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }

    private(set) var flipCount: Int = 0
    private(set) var isGameOver = false
    
    private(set) var score: Int = 0
 
    mutating func chooseCard(at index: Int) {
        assert(cards.indices.contains(index), "Concentration.chooseCard(at: \(index)): chosen index not in the cards")
        flipCount += 1
        
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaceUpCard, matchIndex != index {
                //check if cards match
                if cards[matchIndex] == cards[index] {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    score += 2
                } else {
                    if cards[index].wasPreviouslySeen == true {
                        score -= 1
                    }
                }
                cards[index].isFaceUp = true
            } else {
                indexOfOneAndOnlyFaceUpCard = index
            }
            
            cards[index].wasPreviouslySeen = true
        }
        
        checkIfOver()
    }
    
    mutating private func checkIfOver() {
        var isGameOver = true
        for card in cards {
            if card.isMatched == false {
                isGameOver = false
            }
        }
                
        self.isGameOver = isGameOver
    }
    
    init(numberOfPairsOfCards: Int) {
        assert(numberOfPairsOfCards > 0, "Concentration.init(at: \(numberOfPairsOfCards)): you must have at least one pair of cards")

        for _ in 1...numberOfPairsOfCards {
            let card = Card()
            //when you assign a struct to another variable it copies the struct
                    //let matchingCard = card
            
            //putting things in an array also copies them!
            // so actually, in this func we have 3 cards!!
                    //cards.append(card)
                    //cards.append(card)
            
            cards += [card, card]
            cards.shuffle()
        }
    }
}

extension Collection {
    var oneAndOnly: Element? {
        return count == 1 ? first : nil
    }
}
